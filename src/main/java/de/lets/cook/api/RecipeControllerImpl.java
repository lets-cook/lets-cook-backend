package de.lets.cook.api;

import de.lets.cook.model.Recipe;
import de.lets.cook.model.dto.*;
import de.lets.cook.service.RecipeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class RecipeControllerImpl implements RecipeController {

    private final RecipeService recipeService;
    private final ModelMapper modelMapper;

    @Autowired
    public RecipeControllerImpl(RecipeService recipeService, ModelMapper modelMapper) {
        this.recipeService = recipeService;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<RecipeNoImageDto> getAllRecipes() {
        return mapAll(recipeService.getAllRecipes(), RecipeNoImageDto.class);
    }

    @Override
    public RecipeNoImageDto getRecipeById(String id) {
        return map(recipeService.getRecipeById(id), RecipeNoImageDto.class);
    }

    @Override
    public RecipeNoImageDto addRecipe(@Valid RecipeImageDto recipe) {
        Recipe recipeEntity = map(recipe, Recipe.class);
        Recipe insertedRecipe = recipeService.insertRecipe(recipeEntity);
        return map(insertedRecipe, RecipeNoImageDto.class);
    }

    @Override
    public ImageDto getImageById(String id) {
        return map(recipeService.getRecipeById(id), ImageDto.class);
    }

    @Override
    public RecipeDto updateRecipe(@Valid RecipeDto recipe) {
        Recipe recipeEntity = map(recipe, Recipe.class);
        Recipe updatedRecipe = recipeService.updateRecipe(recipeEntity);
        return map(updatedRecipe, RecipeDto.class);
    }

    @Override
    public DeleteStatusDto deleteRecipeById(String id) {
        recipeService.deleteRecipeById(id);
        return new DeleteStatusDto(id, "Deleted recipe with id [" + id + "] in database", 200);
    }

    private <D, T> D map(final T object, Class<D> outClass) {
        return modelMapper.map(object, outClass);
    }

    private <D, T> List<D> mapAll(final Collection<T> objectList, Class<D> outCLass) {
        return objectList.stream()
                .map(entity -> map(entity, outCLass))
                .collect(Collectors.toList());
    }
}
