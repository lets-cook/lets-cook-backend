package de.lets.cook.api;

import de.lets.cook.model.dto.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/api/recipes")
public interface RecipeController {

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    List<RecipeNoImageDto> getAllRecipes();

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    RecipeNoImageDto getRecipeById(@PathVariable("id") String id);

    @GetMapping(value = "/{id}/image", produces = MediaType.APPLICATION_JSON_VALUE)
    ImageDto getImageById(@PathVariable("id") String id);

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    RecipeNoImageDto addRecipe(@Valid @RequestBody RecipeImageDto recipe);

    @PutMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    RecipeDto updateRecipe(@Valid @RequestBody RecipeDto recipe);

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    DeleteStatusDto deleteRecipeById(@PathVariable("id") String id);
}
