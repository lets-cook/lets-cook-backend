package de.lets.cook.service;

import de.lets.cook.model.Recipe;

import java.util.List;

public interface RecipeService {

    List<Recipe> getAllRecipes();

    Recipe getRecipeById(String id);

    Recipe insertRecipe(Recipe recipe);

    void deleteRecipeById(String id);

    Recipe updateRecipe(Recipe recipe);
}
