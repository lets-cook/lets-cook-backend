package de.lets.cook.service;

import de.lets.cook.exception.RecipeNotFoundException;
import de.lets.cook.model.Recipe;
import de.lets.cook.mongo.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    @Override
    public List<Recipe> getAllRecipes() {
        return recipeRepository.findAll();
    }

    @Override
    public Recipe getRecipeById(String id) {
        Optional<Recipe> optRecipe = recipeRepository.findById(id);
        if (optRecipe.isPresent()) {
            return optRecipe.get();
        }
        throw new RecipeNotFoundException("Recipe with id [" + id + "] not found in database");
    }

    @Override
    public Recipe insertRecipe(Recipe recipe) {
        return recipeRepository.insert(recipe);
    }

    @Override
    public Recipe updateRecipe(Recipe recipe) {
        //prüfen, ob das zu ändernde Rezept in der Datenbank vorhanden ist
        getRecipeById(recipe.getId());
        return recipeRepository.save(recipe);
    }

    @Override
    public void deleteRecipeById(String id) {
        Recipe recipe = getRecipeById(id);
        recipeRepository.delete(recipe);
    }
}
