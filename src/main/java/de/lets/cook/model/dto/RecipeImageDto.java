package de.lets.cook.model.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class RecipeImageDto {

    @NotBlank(message = "is empty or null")
    String title;
    @Min(value = 1, message = "is 0")
    @NotNull(message = "is null")
    Integer people;
    @NotBlank(message = "is empty or null")
    String description;
    @NotNull(message = "is null")
    List<IngredientDto> ingredients;
    @NotNull(message = "is null")
    List<InstructionDto> instructions;
    String image;

    public RecipeImageDto() {
    }

    public RecipeImageDto(@NotBlank(message = "is empty or null") String title, @Min(value = 1, message = "is 0") @NotNull(message = "is null") Integer people, @NotBlank(message = "is empty or null") String description, @NotNull(message = "is null") List<IngredientDto> ingredients, @NotNull(message = "is null") List<InstructionDto> instructions, String image) {
        this.title = title;
        this.people = people;
        this.description = description;
        this.ingredients = ingredients;
        this.instructions = instructions;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPeople() {
        return people;
    }

    public void setPeople(Integer people) {
        this.people = people;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<IngredientDto> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientDto> ingredients) {
        this.ingredients = ingredients;
    }

    public List<InstructionDto> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<InstructionDto> instructions) {
        this.instructions = instructions;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
