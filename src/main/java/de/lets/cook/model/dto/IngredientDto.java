package de.lets.cook.model.dto;

import javax.validation.constraints.NotBlank;

public class IngredientDto {

    Double amount;
    String unit;
    @NotBlank(message = "is empty or null")
    String name;

    public IngredientDto() {
    }

    public IngredientDto(Double amount, String unit, @NotBlank String name) {
        this.amount = amount;
        this.unit = unit;
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
