package de.lets.cook.model.dto;

public class ImageDto {

    String id;
    String image;

    public ImageDto() {
    }

    public ImageDto(String id, String image) {
        this.id = id;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
