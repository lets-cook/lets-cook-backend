package de.lets.cook.model.dto;

import java.text.Collator;
import java.util.List;

import static java.util.Collections.sort;

public class ValidationErrorDto {

    private String type;
    private List<String> errors;
    private Integer status;

    public ValidationErrorDto() {
    }

    public ValidationErrorDto(String type, List<String> errors, Integer status) {
        this.type = type;
        this.errors = errors;
        sort(this.errors, Collator.getInstance());
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
