package de.lets.cook.model.dto;

import javax.validation.constraints.NotBlank;

public class InstructionDto {

    @NotBlank(message = "is empty or null")
    String text;

    public InstructionDto() {
    }

    public InstructionDto(@NotBlank String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
