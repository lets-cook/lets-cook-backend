package de.lets.cook.model.dto;

public class DeleteStatusDto {

    String id;
    String message;
    Integer status;

    public DeleteStatusDto() {
    }

    public DeleteStatusDto(String id, String message, Integer status) {
        this.id = id;
        this.message = message;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
