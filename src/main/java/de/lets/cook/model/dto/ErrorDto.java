package de.lets.cook.model.dto;

public class ErrorDto {

    private String type;
    private String message;
    private Integer status;

    public ErrorDto() {
    }

    public ErrorDto(String type, String message, Integer status) {
        this.type = type;
        this.message = message;
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
