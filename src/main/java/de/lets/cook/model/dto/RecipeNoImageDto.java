package de.lets.cook.model.dto;

import java.util.List;

public class RecipeNoImageDto {

    String id;
    String title;
    Integer people;
    String description;
    List<IngredientDto> ingredients;
    List<InstructionDto> instructions;

    public RecipeNoImageDto() {
    }

    public RecipeNoImageDto(String id, String title, Integer people, String description, List<IngredientDto> ingredients, List<InstructionDto> instructions) {
        this.id = id;
        this.title = title;
        this.people = people;
        this.description = description;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPeople() {
        return people;
    }

    public void setPeople(Integer people) {
        this.people = people;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<IngredientDto> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientDto> ingredients) {
        this.ingredients = ingredients;
    }

    public List<InstructionDto> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<InstructionDto> instructions) {
        this.instructions = instructions;
    }
}
