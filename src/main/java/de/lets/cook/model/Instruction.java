package de.lets.cook.model;

public class Instruction {

    String text;

    public Instruction() {
    }

    public Instruction(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
