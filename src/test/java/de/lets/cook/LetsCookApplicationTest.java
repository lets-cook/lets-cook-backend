package de.lets.cook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.lets.cook.config.ApplicationConfig;
import de.lets.cook.model.Ingredient;
import de.lets.cook.model.Instruction;
import de.lets.cook.model.Recipe;
import de.lets.cook.model.dto.*;
import de.lets.cook.mongo.RecipeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableMongoRepositories
@ActiveProfiles("test")
@Import(ApplicationConfig.class)
class LetsCookApplicationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${api.uri.recipes}")
    private String path;

    @Value("${test.data.id}")
    private String testId;
    @Value("${test.data.title}")
    private String testTitle;
    @Value("${test.data.description}")
    private String testDescription;
    @Value("${test.data.ingredient}")
    private String testIngredient;
    @Value("${test.data.amount}")
    private Double testAmount;
    @Value("${test.data.instruction}")
    private String testInstruction;
    @Value("${test.data.image}")
    private String testImage;

    @BeforeEach
    void deleteData() {
        recipeRepository.deleteAll();
    }

    @Test
    void contextLoad() {
        //spring magic appears here
    }

    @Test
    void getAllRecipesReturnsZeroRecipesAndStatus200Test() throws JsonProcessingException {
        //arrange
        //act
        ResponseEntity<String> response = restTemplate.getForEntity(createURLWithPort(path), String.class);
        //assert
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(new ArrayList<>()));
    }

    @Test
    void getAllRecipesReturnsOneRecipeAndStatus200Test() throws JsonProcessingException {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeNoImageDto recipeNoImageDto = new RecipeNoImageDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto);

        List<RecipeNoImageDto> recipesDto = new ArrayList<>();
        recipesDto.add(recipeNoImageDto);

        recipeRepository.insert(recipe);
        //act
        ResponseEntity<String> response = restTemplate.getForEntity(createURLWithPort(path), String.class);
        //assert
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(recipesDto));
    }

    @Test
    void getAllRecipesReturnsThreeRecipesAndStatus200Test() throws JsonProcessingException {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe testRecipe01 = new Recipe("1", "test-title-01", 1, testDescription, ingredients, instructions, "1111");
        Recipe testRecipe02 = new Recipe("2", "test-title-02", 2, testDescription, ingredients, instructions, "2222");
        Recipe testRecipe03 = new Recipe("3", "test-title-03", 3, testDescription, ingredients, instructions, "3333");
        //dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeNoImageDto testRecipe01Dto = new RecipeNoImageDto("1", "test-title-01", 1, testDescription, ingredientsDto, instructionsDto);
        RecipeNoImageDto testRecipe02Dto = new RecipeNoImageDto("2", "test-title-02", 2, testDescription, ingredientsDto, instructionsDto);
        RecipeNoImageDto testRecipe03Dto = new RecipeNoImageDto("3", "test-title-03", 3, testDescription, ingredientsDto, instructionsDto);

        List<RecipeNoImageDto> recipesDto = new ArrayList<>();
        recipesDto.add(testRecipe01Dto);
        recipesDto.add(testRecipe02Dto);
        recipesDto.add(testRecipe03Dto);

        recipeRepository.insert(testRecipe01);
        recipeRepository.insert(testRecipe02);
        recipeRepository.insert(testRecipe03);
        //act
        ResponseEntity<String> response = restTemplate.getForEntity(createURLWithPort(path), String.class);
        //assert
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(recipesDto));
    }

    @Test
    void getRecipeByIdReturnsErrorAndStatus404Test() throws JsonProcessingException {
        //arrange
        String errorMsg = "Recipe with id [" + testId + "] not found in database";
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", errorMsg, 404);
        //act
        ResponseEntity<String> response = restTemplate.getForEntity(createURLWithPort(path + testId), String.class);
        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    @Test
    void getRecipeByIdReturnsRecipeAndStatus200Test() throws JsonProcessingException {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeNoImageDto recipeNoImageDto = new RecipeNoImageDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto);

        recipeRepository.insert(recipe);
        //act
        ResponseEntity<String> response = restTemplate.getForEntity(createURLWithPort(path + testId), String.class);
        //assert
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(recipeNoImageDto));
    }

    @Test
    void getImageByIdReturnsErrorAndStatus404Test() throws JsonProcessingException {
        //arrange
        String errorMsg = "Recipe with id [" + testId + "] not found in database";
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", errorMsg, 404);
        //act
        ResponseEntity<String> response = restTemplate.getForEntity(createURLWithPort(path + testId + "/image"), String.class);
        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    @Test
    void getImageByIdReturnsImageAndStatus200Test() throws JsonProcessingException {
        //arrange
        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, null, null, testImage);
        ImageDto imageDto = new ImageDto(testId, testImage);

        recipeRepository.insert(recipe);
        //act
        ResponseEntity<String> response = restTemplate.getForEntity(createURLWithPort(path + testId + "/image"), String.class);
        //assert
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(imageDto));
    }

    @Test
    void addRecipeReturnsRecipeAndStatus200() throws JsonProcessingException {
        //arrange
        //post dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeImageDto postRecipeDto = new RecipeImageDto(testTitle, 1, testDescription, ingredientsDto, instructionsDto, testImage);
        //response dto
        RecipeNoImageDto responseRecipeNoImageDto = new RecipeNoImageDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto);

        //act
        ResponseEntity<String> response = restTemplate.postForEntity(createURLWithPort(path + "/"), postRecipeDto, String.class);

        //workaround because the insert generates a random id every time
        List<Recipe> resRecipes = recipeRepository.findAll();
        Recipe resRecipe = resRecipes.get(0);
        responseRecipeNoImageDto.setId(resRecipe.getId());

        //assert
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(responseRecipeNoImageDto));
    }

    @Test
    void addRecipeReturnsValidationErrorAndStatus400() throws JsonProcessingException {
        //arrange
        //post dto
        RecipeImageDto postRecipeDto = new RecipeImageDto("", 0, "", null, null, testImage);
        //response dto
        List<String> errors = new ArrayList<>();
        errors.add("title is empty or null");
        errors.add("people is 0");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);

        //act
        ResponseEntity<String> response = restTemplate.postForEntity(createURLWithPort(path + "/"), postRecipeDto, String.class);

        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    @Test
    void addRecipeNullDtoReturnsValidationErrorAndStatus400() throws JsonProcessingException {
        //arrange
        //post dto
        RecipeImageDto postRecipeDto = new RecipeImageDto(null, null, null, null, null, null);
        //response dto
        List<String> errors = new ArrayList<>();
        errors.add("title is empty or null");
        errors.add("people is null");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);

        //act
        ResponseEntity<String> response = restTemplate.postForEntity(createURLWithPort(path + "/"), postRecipeDto, String.class);

        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    @Test
    void updateRecipeReturnsRecipeAndStatus200() throws JsonProcessingException {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //post dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "kg", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto("updated-instruction");
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeDto postRecipeDto = new RecipeDto(testId, "updated-title", 2, "updated-description", ingredientsDto, instructionsDto, "updated-image");
        //response dto
        RecipeDto responseRecipeDto = new RecipeDto(testId, "updated-title", 2, "updated-description", ingredientsDto, instructionsDto, "updated-image");

        recipeRepository.insert(recipe);
        //act
        HttpEntity<RecipeDto> requestUpdate = new HttpEntity<>(postRecipeDto);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path + "/"), HttpMethod.PUT, requestUpdate, String.class);
        //assert
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(responseRecipeDto));
    }

    @Test
    void updateRecipeReturnsValidationErrorAndStatus400() throws JsonProcessingException {
        //arrange
        //post dto
        RecipeDto postRecipeDto = new RecipeDto("", "", 0, "", null, null, testImage);
        //response dto
        List<String> errors = new ArrayList<>();
        errors.add("id is empty or null");
        errors.add("title is empty or null");
        errors.add("people is 0");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);
        //act
        HttpEntity<RecipeDto> requestUpdate = new HttpEntity<>(postRecipeDto);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path + "/"), HttpMethod.PUT, requestUpdate, String.class);
        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    @Test
    void updateRecipeNullDtoReturnsValidationErrorAndStatus400() throws JsonProcessingException {
        //arrange
        //post dto
        RecipeDto postRecipeDto = new RecipeDto(null, null, null, null, null, null, null);
        //response dto
        List<String> errors = new ArrayList<>();
        errors.add("id is empty or null");
        errors.add("title is empty or null");
        errors.add("people is null");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);
        //act
        HttpEntity<RecipeDto> requestUpdate = new HttpEntity<>(postRecipeDto);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path + "/"), HttpMethod.PUT, requestUpdate, String.class);
        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    @Test
    void updateRecipeReturnsStatus404() throws JsonProcessingException {
        //arrange
        //post dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeDto postRecipeDto = new RecipeDto(testId, testTitle, 2, testDescription, ingredientsDto, instructionsDto, testImage);
        //response dto
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", "Recipe with id [" + testId + "] not found in database", 404);
        //act
        HttpEntity<RecipeDto> requestUpdate = new HttpEntity<>(postRecipeDto);
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path + "/"), HttpMethod.PUT, requestUpdate, String.class);
        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    @Test
    void deleteRecipeByIdReturnsStatus200() throws JsonProcessingException {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //response dto
        DeleteStatusDto deleteStatusDto = new DeleteStatusDto(testId, "Deleted recipe with id [" + testId + "] in database", 200);

        recipeRepository.insert(recipe);
        //act
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path + "/" + testId), HttpMethod.DELETE, null, String.class);
        //assert
        Optional<Recipe> resultRecipe = recipeRepository.findById(testId);
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(deleteStatusDto));
        assertThat(resultRecipe).isNotPresent();
    }

    @Test
    void deleteRecipeByIdReturnsStatus404() throws JsonProcessingException {
        //arrange
        //response dto
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", "Recipe with id [" + testId + "] not found in database", 404);
        //act
        ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path + "/" + testId), HttpMethod.DELETE, null, String.class);
        //assert
        assertThat(response.getStatusCode().is4xxClientError()).isTrue();
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        assertThat(response.getBody()).isEqualTo(objectMapper.writeValueAsString(errorDto));
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
