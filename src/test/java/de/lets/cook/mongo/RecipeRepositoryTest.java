package de.lets.cook.mongo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.lets.cook.model.Ingredient;
import de.lets.cook.model.Instruction;
import de.lets.cook.model.Recipe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataMongoTest
@ActiveProfiles("test")
class RecipeRepositoryTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private RecipeRepository recipeRepository;
    @Value("${test.data.id}")
    private String testId;
    @Value("${test.data.title}")
    private String testTitle;
    @Value("${test.data.description}")
    private String testDescription;
    @Value("${test.data.ingredient}")
    private String testIngredient;
    @Value("${test.data.amount}")
    private Double testAmount;
    @Value("${test.data.instruction}")
    private String testInstruction;
    @Value("${test.data.image}")
    private String testImage;

    @BeforeEach
    void deleteData() {
        recipeRepository.deleteAll();
    }

    @Test
    void findAllReturnsEmptyListTest() {
        //act
        List<Recipe> recipes = recipeRepository.findAll();
        //assert
        assertThat(recipes).isEmpty();
    }

    @Test
    void insertAndFindAllResturnsOneRecipeTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipe);
        //act
        recipeRepository.insert(recipe);
        List<Recipe> resultRecipes = recipeRepository.findAll();
        //assert
        assertThat(resultRecipes.isEmpty()).isFalse();
        assertThat(resultRecipes.size()).isEqualTo(1);
        assertThat(objectMapper.writeValueAsString(recipes)).isEqualTo(objectMapper.writeValueAsString(resultRecipes));
    }

    @Test
    void insertAndFindAllReturnsThreeRecipesTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe testRecipe01 = new Recipe("1", "test-title-01", 1, "test-description-01", ingredients, instructions, "1111");
        Recipe testRecipe02 = new Recipe("2", "test-title-02", 2, "test-description-02", ingredients, instructions, "2222");
        Recipe testRecipe03 = new Recipe("3", "test-title-03", 3, "test-description-03", ingredients, instructions, "3333");

        List<Recipe> recipes = new ArrayList<>();
        recipes.add(testRecipe01);
        recipes.add(testRecipe02);
        recipes.add(testRecipe03);
        //act
        recipeRepository.insert(testRecipe01);
        recipeRepository.insert(testRecipe02);
        recipeRepository.insert(testRecipe03);
        List<Recipe> resultRecipes = recipeRepository.findAll();
        //assert
        assertThat(resultRecipes.isEmpty()).isFalse();
        assertThat(resultRecipes.size()).isEqualTo(3);
        assertThat(objectMapper.writeValueAsString(recipes)).isEqualTo(objectMapper.writeValueAsString(resultRecipes));
    }

    @Test
    void insertAndFindByIdTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //act
        recipeRepository.insert(recipe);
        Optional<Recipe> resultRecipe = recipeRepository.findById(testId);
        //assert
        assertThat(resultRecipe).isPresent();
        assertThat(objectMapper.writeValueAsString(recipe)).isEqualTo(objectMapper.writeValueAsString(resultRecipe.get()));
    }

    @Test
    void findByIdReturnsNoRecipeTest() {
        //act
        Optional<Recipe> resultRecipe = recipeRepository.findById(testId);
        //assert
        assertThat(resultRecipe).isNotPresent();
    }

    @Test
    void insertAndDeleteByIdTest() {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //act
        recipeRepository.insert(recipe);
        recipeRepository.delete(recipe);
        Optional<Recipe> resultRecipe = recipeRepository.findById(testId);
        //assert
        assertThat(resultRecipe).isNotPresent();
    }

    @Test
    void insertAndUpdateTest() throws JsonProcessingException {
        //arrange
        //inserted recipe
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //updated recipe
        Recipe updatedRecipe = new Recipe(testId, "update-title", 2, "updated-recipe", ingredients, instructions, "updated-image");
        //act
        recipeRepository.insert(recipe);
        recipeRepository.save(updatedRecipe);
        Optional<Recipe> resultRecipe = recipeRepository.findById(testId);
        //assert
        assertThat(resultRecipe).isPresent();
        assertThat(objectMapper.writeValueAsString(updatedRecipe)).isEqualTo(objectMapper.writeValueAsString(resultRecipe.get()));
    }
}