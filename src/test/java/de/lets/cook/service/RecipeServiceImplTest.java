package de.lets.cook.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.lets.cook.exception.RecipeNotFoundException;
import de.lets.cook.model.Ingredient;
import de.lets.cook.model.Instruction;
import de.lets.cook.model.Recipe;
import de.lets.cook.mongo.RecipeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@TestPropertySource(locations = {"classpath:application-test.yaml"})
@ContextConfiguration(initializers = {ConfigFileApplicationContextInitializer.class})
class RecipeServiceImplTest {

    private RecipeService recipeService;

    private ObjectMapper objectMapper;

    @Mock
    private RecipeRepository recipeRepository;

    @Value("${test.data.id}")
    private String testId;
    @Value("${test.data.title}")
    private String testTitle;
    @Value("${test.data.description}")
    private String testDescription;
    @Value("${test.data.ingredient}")
    private String testIngredient;
    @Value("${test.data.amount}")
    private Double testAmount;
    @Value("${test.data.instruction}")
    private String testInstruction;
    @Value("${test.data.image}")
    private String testImage;

    @BeforeEach
    void setUp() {
        recipeService = new RecipeServiceImpl(recipeRepository);
        objectMapper = new ObjectMapper();
    }

    @Test
    void getAllRecipesReturnsEmptyArrayTest() {
        //arrange
        Mockito.when(recipeRepository.findAll()).thenReturn(new ArrayList<>());
        //act
        List<Recipe> resultRecipes = recipeService.getAllRecipes();
        //assert
        assertThat(resultRecipes).isEmpty();
    }

    @Test
    void getAllRecipesReturnsOneRecipeTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipe);

        Mockito.when(recipeRepository.findAll()).thenReturn(recipes);
        //act
        List<Recipe> resultRecipes = recipeService.getAllRecipes();
        //assert
        assertThat(recipes.isEmpty()).isFalse();
        assertThat(recipes.size()).isEqualTo(1);
        assertThat(objectMapper.writeValueAsString(recipes)).isEqualTo(objectMapper.writeValueAsString(resultRecipes));
    }

    @Test
    void getAllRecipesReturnsThreeRecipeTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe testRecipe01 = new Recipe("1", "test-title-01", 1, "test-description-01", ingredients, instructions, "1111");
        Recipe testRecipe02 = new Recipe("2", "test-title-02", 2, "test-description-02", ingredients, instructions, "2222");
        Recipe testRecipe03 = new Recipe("3", "test-title-03", 3, "test-description-03", ingredients, instructions, "3333");

        List<Recipe> recipes = new ArrayList<>();
        recipes.add(testRecipe01);
        recipes.add(testRecipe02);
        recipes.add(testRecipe03);

        Mockito.when(recipeRepository.findAll()).thenReturn(recipes);
        //act
        List<Recipe> resultRecipes = recipeService.getAllRecipes();
        //assert
        assertThat(recipes.isEmpty()).isFalse();
        assertThat(resultRecipes.size()).isEqualTo(3);
        assertThat(objectMapper.writeValueAsString(recipes)).isEqualTo(objectMapper.writeValueAsString(resultRecipes));
    }

    @Test
    void getRecipeByIdTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);

        Mockito.when(recipeRepository.findById(testId)).thenReturn(Optional.of(recipe));
        //act
        Recipe resultRecipe = recipeService.getRecipeById(testId);
        //assert
        assertThat(objectMapper.writeValueAsString(recipe)).isEqualTo(objectMapper.writeValueAsString(resultRecipe));
    }

    @Test
    void getRecipeByIdThrowsRecipeNotFoundExceptionTest() {
        //arrange
        String expectedMessage = "Recipe with id [" + testId + "] not found in database";

        Mockito.when(recipeRepository.findById(testId)).thenReturn(Optional.empty());
        //act
        Exception exception = assertThrows(RecipeNotFoundException.class, () -> recipeService.getRecipeById(testId));
        //assert
        assertThat(expectedMessage).isEqualTo(exception.getMessage());
    }

    @Test
    void insertRecipeTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);

        Mockito.when(recipeRepository.insert(recipe)).thenReturn(recipe);
        //act
        Recipe resultRecipe = recipeService.insertRecipe(recipe);
        //assert
        assertThat(objectMapper.writeValueAsString(recipe)).isEqualTo(objectMapper.writeValueAsString(resultRecipe));
    }

    @Test
    void updateRecipeTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);

        Mockito.when(recipeRepository.findById(testId)).thenReturn(Optional.of(recipe));
        Mockito.when(recipeRepository.save(recipe)).thenReturn(recipe);
        //act
        Recipe resultRecipe = recipeService.updateRecipe(recipe);
        //assert
        assertThat(objectMapper.writeValueAsString(recipe)).isEqualTo(objectMapper.writeValueAsString(resultRecipe));
    }

    @Test
    void updateRecipeThrowsRecipeNotFoundExceptionTest() throws JsonProcessingException {
        //arrange
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);

        String expectedMessage = "Recipe with id [" + testId + "] not found in database";

        Mockito.when(recipeRepository.findById(testId)).thenReturn(Optional.empty());
        //act
        Exception exception = assertThrows(RecipeNotFoundException.class, () -> recipeService.updateRecipe(recipe));
        //assert
        assertThat(expectedMessage).isEqualTo(exception.getMessage());
    }

    @Test
    void deleteRecipeByIdThrowsRecipeNotFoundExceptionTest() {
        //arrange
        String expectedMessage = "Recipe with id [" + testId + "] not found in database";

        Mockito.when(recipeRepository.findById(testId)).thenReturn(Optional.empty());
        //act
        Exception exception = assertThrows(RecipeNotFoundException.class, () -> recipeService.deleteRecipeById(testId));
        //assert
        assertThat(expectedMessage).isEqualTo(exception.getMessage());
    }
}
