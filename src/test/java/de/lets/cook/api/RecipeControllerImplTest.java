package de.lets.cook.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.lets.cook.config.ApplicationConfig;
import de.lets.cook.exception.RecipeNotFoundException;
import de.lets.cook.model.Ingredient;
import de.lets.cook.model.Instruction;
import de.lets.cook.model.Recipe;
import de.lets.cook.model.dto.*;
import de.lets.cook.service.RecipeService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RecipeController.class)
@ActiveProfiles("test")
@Import(ApplicationConfig.class)
class RecipeControllerImplTest {

    private static final String EXPECTED_CONTENT_TYPE = MediaType.APPLICATION_JSON_VALUE;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private RecipeService recipeService;
    @Value("${api.uri.recipes}")
    private String path;
    @Value("${test.data.id}")
    private String testId;
    @Value("${test.data.title}")
    private String testTitle;
    @Value("${test.data.description}")
    private String testDescription;
    @Value("${test.data.ingredient}")
    private String testIngredient;
    @Value("${test.data.amount}")
    private Double testAmount;
    @Value("${test.data.instruction}")
    private String testInstruction;
    @Value("${test.data.image}")
    private String testImage;

    @Test
    void getAllRecipesReturnsEmptyArrayAndStatus200Test() throws Exception {
        //arrange
        Mockito.when(recipeService.getAllRecipes()).thenReturn(new ArrayList<>());
        //act
        mockMvc.perform(get(path)).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json("[]"));
    }

    @Test
    void getAllRecipesReturnsOneRecipeAndStatus200Test() throws Exception {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipe);
        //dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeNoImageDto recipeNoImageDto = new RecipeNoImageDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto);
        List<RecipeNoImageDto> recipesDto = new ArrayList<>();
        recipesDto.add(recipeNoImageDto);

        Mockito.when(recipeService.getAllRecipes()).thenReturn(recipes);
        //act
        mockMvc.perform(get(path)).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(recipesDto)));
    }

    @Test
    void getAllRecipesReturnsThreeRecipesAndStatus200Test() throws Exception {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe testRecipe01 = new Recipe("1", "test-title-01", 1, "test-description-01", ingredients, instructions, "1111");
        Recipe testRecipe02 = new Recipe("2", "test-title-02", 2, "test-description-02", ingredients, instructions, "2222");
        Recipe testRecipe03 = new Recipe("3", "test-title-03", 3, "test-description-03", ingredients, instructions, "3333");

        List<Recipe> recipes = new ArrayList<>();
        recipes.add(testRecipe01);
        recipes.add(testRecipe02);
        recipes.add(testRecipe03);

        //dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeNoImageDto testRecipe01Dto = new RecipeNoImageDto("1", "test-title-01", 1, "test-description-01", ingredientsDto, instructionsDto);
        RecipeNoImageDto testRecipe02Dto = new RecipeNoImageDto("2", "test-title-02", 2, "test-description-02", ingredientsDto, instructionsDto);
        RecipeNoImageDto testRecipe03Dto = new RecipeNoImageDto("3", "test-title-03", 3, "test-description-03", ingredientsDto, instructionsDto);

        List<RecipeNoImageDto> recipesDto = new ArrayList<>();
        recipesDto.add(testRecipe01Dto);
        recipesDto.add(testRecipe02Dto);
        recipesDto.add(testRecipe03Dto);

        Mockito.when(recipeService.getAllRecipes()).thenReturn(recipes);
        //act
        mockMvc.perform(get(path)).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(recipesDto)));
    }

    @Test
    void getRecipeByIdReturnsStatus404Test() throws Exception {
        //arrange
        String errorMsg = "Recipe with id [" + testId + "] not found in database";
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", errorMsg, 404);

        Mockito.when(recipeService.getRecipeById(testId)).thenThrow(new RecipeNotFoundException(errorMsg));
        //act
        mockMvc.perform(get(path + "/" + testId)).
                //Assert
                        andExpect(status().isNotFound()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }

    @Test
    void getRecipeByIdReturnsOneRecipeAndStatus200Test() throws Exception {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeNoImageDto recipeNoImageDto = new RecipeNoImageDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto);

        Mockito.when(recipeService.getRecipeById(testId)).thenReturn(recipe);
        //act
        mockMvc.perform(get(path + "/" + testId)).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(recipeNoImageDto)));
    }

    @Test
    void getImageByIdReturnsStatus404Test() throws Exception {
        //arrange
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", "Recipe with id [" + testId + "] not found in database", 404);

        Mockito.when(recipeService.getRecipeById(testId)).thenThrow(new RecipeNotFoundException("Recipe with id [" + testId + "] not found in database"));
        //act
        mockMvc.perform(get(path + "/" + testId + "/image")).
                //Assert
                        andExpect(status().isNotFound()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }

    @Test
    void getImageByIdReturnsOneImageAndStatus200Test() throws Exception {
        //arrange
        //entity
        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, null, null, testImage);
        //dto
        ImageDto imageDto = new ImageDto(testId, testImage);

        Mockito.when(recipeService.getRecipeById(testId)).thenReturn(recipe);
        //act
        mockMvc.perform(get(path + "/" + testId + "/image")).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(imageDto)));
    }

    @Test
    void addRecipeReturns200Test() throws Exception {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //post dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeImageDto postRecipeDto = new RecipeImageDto(testTitle, 1, testDescription, ingredientsDto, instructionsDto, testImage);
        //dto
        RecipeNoImageDto recipeNoImageDto = new RecipeNoImageDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto);

        Mockito.when(recipeService.insertRecipe(any(Recipe.class))).thenReturn(recipe);
        //act
        mockMvc.perform(post(path + "/").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(postRecipeDto))).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(recipeNoImageDto)));
    }

    @Test
    void addRecipeReturnsStatus400Test() throws Exception {
        //arrange
        //post dto
        RecipeImageDto postRecipeDto = new RecipeImageDto("", 0, "", null, null, testImage);
        //error dto
        List<String> errors = new ArrayList<>();
        errors.add("title is empty or null");
        errors.add("people is 0");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);
        //act
        mockMvc.perform(post(path + "/").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(postRecipeDto))).
                //assert
                        andExpect(status().isBadRequest()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }

    @Test
    void addRecipeNullDtoReturnsStatus400Test() throws Exception {
        //arrange
        //post dto
        RecipeImageDto postRecipeDto = new RecipeImageDto(null, null, null, null, null, null);
        //error dto
        List<String> errors = new ArrayList<>();
        errors.add("title is empty or null");
        errors.add("people is null");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);
        //act
        mockMvc.perform(post(path + "/").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(postRecipeDto))).
                //assert
                        andExpect(status().isBadRequest()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }

    @Test
    void updateRecipeReturns200Test() throws Exception {
        //arrange
        //entity
        Ingredient ingredient = new Ingredient(testAmount, "g", testIngredient);
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredient);

        Instruction instruction = new Instruction(testInstruction);
        List<Instruction> instructions = new ArrayList<>();
        instructions.add(instruction);

        Recipe recipe = new Recipe(testId, testTitle, 1, testDescription, ingredients, instructions, testImage);
        //post dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeDto postRecipeDto = new RecipeDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto, testImage);
        //dto
        RecipeDto recipeDto = new RecipeDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto, testImage);

        Mockito.when(recipeService.updateRecipe(any(Recipe.class))).thenReturn(recipe);
        //act
        mockMvc.perform(put(path + "/").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(postRecipeDto))).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(recipeDto)));
    }

    @Test
    void updateRecipeReturnsStatus400Test() throws Exception {
        //arrange
        //post dto
        RecipeDto postRecipeDto = new RecipeDto("", "", 0, "", null, null, testImage);
        //error dto
        List<String> errors = new ArrayList<>();
        errors.add("id is empty or null");
        errors.add("title is empty or null");
        errors.add("people is 0");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);
        //act
        mockMvc.perform(put(path + "/").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(postRecipeDto))).
                //assert
                        andExpect(status().isBadRequest()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }

    @Test
    void updateRecipeNullDtoReturnsStatus400Test() throws Exception {
        //arrange
        //post dto
        RecipeDto postRecipeDto = new RecipeDto(null, null, null, null, null, null, null);
        //error dto
        List<String> errors = new ArrayList<>();
        errors.add("id is empty or null");
        errors.add("title is empty or null");
        errors.add("people is null");
        errors.add("description is empty or null");
        errors.add("ingredients is null");
        errors.add("instructions is null");
        ValidationErrorDto errorDto = new ValidationErrorDto("MethodArgumentNotValidException", errors, 400);
        //act
        mockMvc.perform(put(path + "/").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(postRecipeDto))).
                //assert
                        andExpect(status().isBadRequest()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }

    @Test
    void updateRecipeReturns404Test() throws Exception {
        //arrange
        //post dto
        IngredientDto ingredientDto = new IngredientDto(testAmount, "g", testIngredient);
        List<IngredientDto> ingredientsDto = new ArrayList<>();
        ingredientsDto.add(ingredientDto);

        InstructionDto instructionDto = new InstructionDto(testInstruction);
        List<InstructionDto> instructionsDto = new ArrayList<>();
        instructionsDto.add(instructionDto);

        RecipeDto postRecipeDto = new RecipeDto(testId, testTitle, 1, testDescription, ingredientsDto, instructionsDto, testImage);
        //response dto
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", "Recipe with id [" + testId + "] not found in database", 404);

        Mockito.doThrow(new RecipeNotFoundException("Recipe with id [" + testId + "] not found in database")).when(recipeService).updateRecipe(any(Recipe.class));
        //act
        mockMvc.perform(put(path + "/").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(postRecipeDto))).
                //assert
                        andExpect(status().isNotFound()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }

    @Test
    void deleteRecipeByIdReturns200Test() throws Exception {
        //arrange
        //response dto
        DeleteStatusDto deleteStatusDto = new DeleteStatusDto(testId, "Deleted recipe with id [" + testId + "] in database", 200);

        Mockito.doNothing().when(recipeService).deleteRecipeById(testId);
        //act
        mockMvc.perform(delete(path + "/" + testId)).
                //assert
                        andExpect(status().isOk()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(deleteStatusDto)));
    }

    @Test
    void deleteRecipeByIdReturns404Test() throws Exception {
        //arrange
        //response dto
        ErrorDto errorDto = new ErrorDto("RecipeNotFoundException", "Recipe with id [" + testId + "] not found in database", 404);

        Mockito.doThrow(new RecipeNotFoundException("Recipe with id [" + testId + "] not found in database")).when(recipeService).deleteRecipeById(testId);
        //act
        mockMvc.perform(delete(path + "/" + testId)).
                //assert
                        andExpect(status().isNotFound()).
                andExpect(content().contentType(EXPECTED_CONTENT_TYPE)).
                andExpect(content().json(objectMapper.writeValueAsString(errorDto)));
    }
}